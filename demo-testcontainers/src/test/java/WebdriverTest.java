import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testcontainers.Testcontainers;
import org.testcontainers.containers.BrowserWebDriverContainer;

import java.io.File;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.testcontainers.containers.BrowserWebDriverContainer.VncRecordingMode.RECORD_ALL;

public class WebdriverTest {

    @Rule
    public BrowserWebDriverContainer chrome = new BrowserWebDriverContainer()
            .withCapabilities(new ChromeOptions())
            .withRecordingMode(RECORD_ALL, new File("target"));


    @BeforeClass
    public static void setUpClass() {
        Testcontainers.exposeHostPorts(4200); //forward host port to container
    }

    @Test
    public void simpleWebdriverTest() throws Exception
    {
        final RemoteWebDriver driver = chrome.getWebDriver();

        //ng cli: --disable-host-check
        driver.get("http://host.testcontainers.internal:4200"); // "localhost"
//        driver.get("http://docker.for.mac.host.internal:4200/");
        TimeUnit.SECONDS.sleep(2); //longer video


        final WebElement name = driver.findElement(By.id("customerName"));
                name.sendKeys("Karsten Sitterberg");
        TimeUnit.SECONDS.sleep(1); //longer video
        final WebElement id = driver.findElement(By.id("customerId"));
        id.sendKeys("300000");
        TimeUnit.SECONDS.sleep(1); //longer video

        //old api! 20190811
//        final WebElement button = driver.findElementByCssSelector("section form .btn.btn-primary");
        final WebElement button = driver.findElement(By.cssSelector("section form .btn.btn-primary"));
        button.click();

        final WebElement header = driver.findElement(By.cssSelector(".row h2.col-12"));

        assertThat(header.getText(), is("Premiumangebote"));
        TimeUnit.SECONDS.sleep(2); //longer video
    }

}
