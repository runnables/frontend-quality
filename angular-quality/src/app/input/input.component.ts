import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { first } from 'rxjs/operators';
import { getCustomerState, State } from '../store';
import { SetCustomer } from '../store/customer.actions';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html'
})
export class InputComponent implements OnInit {

  customerForm = new FormGroup({
    name: new FormControl('', [Validators.required]),
    number: new FormControl('', [Validators.required])
  });

  constructor(private router: Router, private store: Store<State>) {
  }

  ngOnInit(): void {
    this.store.pipe(
      select(getCustomerState),
      first()
    )
      .subscribe(customer => this.customerForm.setValue(customer));
  }

  submitForm(): void {
    this.store.dispatch(new SetCustomer(this.customerForm.value));
    this.router.navigateByUrl('/offers');
  }

}
