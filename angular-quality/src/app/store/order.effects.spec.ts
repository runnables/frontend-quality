import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { EMPTY, Observable } from 'rxjs';

import { OrderEffects } from './order.effects';

describe('OrderEffects', () => {
  let actions$: Observable<any>;
  let effects: OrderEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        OrderEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.get(OrderEffects);
  });

  it('should be created', () => {
    actions$ = EMPTY;
    expect(effects).toBeTruthy();
  });
});
